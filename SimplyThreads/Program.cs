﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace SimplyThreads
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("-- Dem threads --");
            Console.WriteLine("Choose your option: nosync / lock / autoreset / delay / mainlast");
            string command = Console.ReadLine();
            Console.WriteLine($"Enter number of threads or whatever for processor count ({Environment.ProcessorCount})");
            int threadCount;
            if(!int.TryParse(Console.ReadLine(), out threadCount))
            {
                threadCount = Environment.ProcessorCount;
            }

            var threads = new List<Thread>();
            var threadSample = new ThreadSample();
            Thread.CurrentThread.Name = "MAIN";

            switch(command)
            {
                case "lock":
                    for (int i = 0; i < threadCount; i++)
                    {
                        threads.Add(new Thread(new ThreadStart(threadSample.SomeActionWithLock)) { Name = $"Thread {i}" });
                    }

                    foreach (var t in threads)
                    {
                        t.Start();
                    }

                    threadSample.SomeActionWithLock();
                    break;
                case "autoreset":
                    for (int i = 0; i < threadCount; i++)
                    {
                        threads.Add(new Thread(new ThreadStart(threadSample.SomeActionAutoReset)) { Name = $"Thread {i}" });
                    }

                    foreach (var t in threads)
                    {
                        t.Start();
                    }

                    threadSample.SomeActionAutoReset();
                    break;
                case "delay":
                    for (int i = 0; i < threadCount; i++)
                    {
                        threads.Add(new Thread(new ThreadStart(threadSample.SomeAction)) { Name = $"Thread {i}" });
                    }

                    foreach (var t in threads)
                    {
                        t.Start();
                        Thread.Sleep(1000);
                    }

                    threadSample.SomeAction();
                    break;
                case "mainlast":
                    for (int i = 0; i < threadCount; i++)
                    {
                        threads.Add(new Thread(new ThreadStart(threadSample.SomeAction)) { Name = $"Thread {i}" });
                    }

                    foreach (var t in threads)
                    {
                        t.Start();
                    }

                    foreach (var t in threads)
                    {
                        t.Join();
                    }

                    threadSample.SomeAction();
                    break;
                default:
                case "nosync":
                    for (int i = 0; i < threadCount; i++)
                    {
                        threads.Add(new Thread(new ThreadStart(threadSample.SomeAction)) { Name = $"Thread {i}" });
                    }

                    foreach (var t in threads)
                    {
                        t.Start();
                    }

                    threadSample.SomeAction();
                    break;
            }

            Console.WriteLine("-- Main thread ended --");
            Console.ReadKey();
        }
    }
}
