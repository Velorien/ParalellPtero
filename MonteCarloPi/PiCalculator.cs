﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;

namespace MonteCarloPi
{
    public static class PiCalculator
    {
        public static Task<PartialResult> ExecuteAsync(int pointsCount)
        {
            return Task.Run(() => Calculate(pointsCount));
        }

        private static PartialResult Calculate(int pointsCount)
        {
            var timer = new Stopwatch();
            var result = new PartialResult();
            timer.Start();
            foreach (var point in GetPoints(pointsCount))
            {
                if (IsPointInCircle(point))
                {
                    result.PointsInsideCount++;
                }
            }
            timer.Stop();
            result.TimeElapsed = timer.Elapsed;

            return result;
        }

        private static IEnumerable<Point> GetPoints(int pointsCount)
        {
            var random = new Random();
            for(int i = 0; i < pointsCount; i++)
            {
                yield return new Point(random.NextDouble(), random.NextDouble());
            }
        }

        private static bool IsPointInCircle(Point point) => point.X * point.X + point.Y * point.Y < 1;
    }
}
